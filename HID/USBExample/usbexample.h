/*******************************************************************
Copyright (C) 2011  Fernando Magnano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

***********************************************************************/

#ifndef USBEXAMPLE_H
#define USBEXAMPLE_H

#include <QMainWindow>
#include <qt_windows.h>
#include <QLabel>
#include <QToolButton>
#include <QThread>
#include <qdebug.h>


namespace Ui {
    class USBexample;
}

/*******************************************
*        RECEIVING THREAD CLASS
********************************************/
class ReadThread : public QThread
 {
     Q_OBJECT

 protected:
     void run();
 signals:
      void USBReceived();

 };


/*******************************************
*             MAIN WINDOW CLASS
********************************************/
class USBexample : public QMainWindow
{
    Q_OBJECT

public:
    explicit USBexample(QWidget *parent = 0);
    ~USBexample();


private:
    Ui::USBexample *ui;
    HDEVNOTIFY hDevNotify;
    bool msgp;
    bool UsbRv;
    bool TimerRv;
    bool USBCheck();
    int USBWrite();
    QIcon ledGray;
    QIcon ledGreen;
    QIcon ledYellow;
    QIcon ledRed;
    QLabel *statusLab;
    QToolButton *ledBar;
    ReadThread *rthread;

protected :
    bool winEvent(MSG *msg,long * result);
  //  bool nativeEvent(const QByteArray & eventType, void * message, long * result);
signals:
    void USB_Removed();
    void USB_Arrived();

private slots:
    void in_USBReceived();
    void in_USBArrived();
    void in_USBRemoved();
    void in_timerTimeout();

    void on_checkBoxGreen_clicked();
    void on_checkBoxRed_clicked();
};

#endif // USBEXAMPLE_H
