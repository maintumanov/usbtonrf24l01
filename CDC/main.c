#include <main.h>




#INT_EXT2
void  EXT2_isr(void) 
{

}



/* TODO: Use usb_cdc_putc() to transmit data to the USB
virtual COM port. Use usb_cdc_kbhit() and usb_cdc_getc() to
receive data from the USB virtual COM port. usb_enumerated()
can be used to see if connected to a host and ready to
communicate. */

static void handle_incoming_usb(void)
{

   char c;

   while(usb_cdc_kbhit())
   {
      // since we cannot call usb_cdc_getc() from an ISR unless there
      // is already data in the buffer, we must state machine this routine
      // to continue from previous location.
      c = usb_cdc_getc();
      if (c == 'A') output_high(RED); else output_low(RED);

   }


}

void main()
{
   

   enable_interrupts(INT_EXT2);
   enable_interrupts(GLOBAL);
   usb_init_cs();
   
   output_high(GREEN);

   while(TRUE)
   {
      usb_task();

      //TODO: User Code
   }
   usb_init_cs();

}
