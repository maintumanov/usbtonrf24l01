#-------------------------------------------------
#
# Project created by QtCreator 2011-03-13T23:29:28
#
#-------------------------------------------------

QT       += core widgets

TARGET = USBExample
TEMPLATE = app


SOURCES += main.cpp\
        usbexample.cpp

HEADERS  += usbexample.h

FORMS    += usbexample.ui

RESOURCES += \
    USBexample.qrc

LIBS    += -lsetupapi
