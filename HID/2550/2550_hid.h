
   #include <18F2550.h>
   #fuses HSPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,USBDIV,PLL5,CPUDIV1,VREGEN
   #use delay(clock=20000000)

   //leds ordered from bottom to top
   #DEFINE GREEN PIN_A0  
   #define RED PIN_A1  


#ifndef LED_ON
#define LED_ON(x) output_low(x)
#endif

#ifndef LED_OFF
#define LED_OFF(x) output_high(x)
#endif


