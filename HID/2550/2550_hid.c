#include <2550_hid.h>

#define USB_CONFIG_HID_TX_SIZE   1
#define USB_CONFIG_HID_RX_SIZE   1

#include <pic18_usb.h>   //Microchip PIC18Fxx5x hardware layer for usb.c
#include <usb_desc_hid.h>   //USB Configuration and Device descriptors for this UBS device
#include <usb.c>        //handles usb setup tokens and get descriptor reports


void main(void)
{
   int8 out_data[USB_CONFIG_HID_TX_SIZE];
   int8 in_data[USB_CONFIG_HID_RX_SIZE];
   int8 send_timer=0;

   usb_init_cs();

   while (TRUE)
   {
      usb_task();

      if (usb_enumerated())
      {
         if (!send_timer)
         {
            send_timer=250;
          //  out_data[0] = read_adc();
         //   out_data[1] = BUTTON_PRESSED();
            out_data[0] ++;
            
            usb_put_packet(USB_HID_ENDPOINT, out_data, USB_CONFIG_HID_TX_SIZE, USB_DTS_TOGGLE);

         }
         
         if (usb_kbhit(USB_HID_ENDPOINT))
         {
            usb_get_packet(USB_HID_ENDPOINT, in_data, USB_CONFIG_HID_RX_SIZE);
            if (in_data[0] == 'A') output_high(GREEN);
            if (in_data[0] == 'B') output_low(GREEN);
            if (in_data[0] == 'C') output_high(RED);
            if (in_data[0] == 'D') output_low(RED);

         }
         
         send_timer--;

         delay_ms(1);
      }
   }
}
