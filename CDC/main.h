#include <18F2550.h>
#device ADC=16

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES WDT128                   //Watch Dog Timer uses 1:128 Postscale
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES HSPLL,NOPROTECT,NODEBUG,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay(clock=20000000)

#use FIXED_IO( A_outputs=PIN_A1,PIN_A0 )
#use FIXED_IO( B_outputs=PIN_B4 )

#USE SPI (MASTER, SPI1, ENABLE=PIN_B3, CLOCK_HIGH=0, CLOCK_LOW=0, MODE=0, BITS=8, ENABLE_ACTIVE=0, STREAM=SPI)

#define GREEN   PIN_A0
#define RED   PIN_A1
#define INT2   PIN_B2
#define CE   PIN_B3
#define CSN   PIN_B4

#define USB_CONFIG_VID 0x2400
#define USB_CONFIG_PID 0x000B
#define USB_CONFIG_BUS_POWER 100
#define USB_STRINGS_OVERWRITTEN

char USB_STRING_DESC_OFFSET[]={0,4,24};

char const USB_STRING_DESC[]={
   //string 0 - language
      4,  //length of string index
      0x03,  //descriptor type (STRING)
      0x09,0x04,  //Microsoft Defined for US-English
   //string 1 - manufacturer
      20,  //length of string index
      0x03,  //descriptor type (STRING)
      'S',0,
      'i',0,
      'g',0,
      'n',0,
      'a',0,
      'l',0,
      'N',0,
      'e',0,
      't',0,
   //string 2 - product
      18,  //length of string index
      0x03,  //descriptor type (STRING)
      'N',0,
      'R',0,
      'F',0,
      '2',0,
      '4',0,
      'L',0,
      '0',0,
      '1',0
};

#define USB_CDC_ISR() handle_incoming_usb()
static void handle_incoming_usb(void);

#include <usb_cdc.h>



