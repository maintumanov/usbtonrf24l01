/*******************************************************************
Copyright (C) 2011  Fernando Magnano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

***********************************************************************/
/**********************************************************************
This program is an example of how to use an USB device in Qt.
It is limited only to the HID devices class (joystick, mouse, keyboard...)
It uses the standard Microsoft HID driver. If you use another driver you must know how it works.
It demonstrates how to:
-recognize a device by VID&PID
-read/write
-attach/detach notification
Two examples are provided:
-first example reads the XY position of a joystick
-second example sends a message to the device and waits for the answer.

DO NOT FORGET TO ADD THE FOLLOWING LINE to your .pro file
LIBS    += -lsetupapi
***********************************************************************/
#include "usbexample.h"
#include "ui_usbexample.h"
#include <setupapi.h>
#include <dbt.h>
#include <QMessageBox>
#include <QTimer>

/**************************************************
*                 CONSTANTS
***************************************************/
//CHANGE FOR VID&PID OF YOUR DEVICE
//#define MY_DEVICE_VIDPID  "VID_06D6&PID_0026"  //This is the VID&PID of my joystick
#define MY_DEVICE_VIDPID  "VID_0461&PID_0020"  //This is the VID&PID of my joystick
//Class GUID for HID devices
#define HID_CLASSGUID {0x4d1e55b2, 0xf16f, 0x11cf,{ 0x88, 0xcb, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30}}

/***************************************************
*                  GLOBAL VARIABLES
****************************************************/
HANDLE WriteHandle = INVALID_HANDLE_VALUE;	//Handle to write
HANDLE ReadHandle = INVALID_HANDLE_VALUE;	//Handle to read
bool USBConnected;      //Boolean true when device is connected
//In/Out buffers equal to HID endpoint size + 1
//First byte is for Windows internal use and it is always 0
unsigned char OutputBuffer[65];
unsigned char InputBuffer[65];


/**************************************************
*                 CONSTRUCTOR
***************************************************/
USBexample::USBexample(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::USBexample)
{
    ui->setupUi(this);

    msgp = false;       //Boolean true after the first WM_PAINT Windows event
    UsbRv = false;      // Boolean true when a packet is received
    TimerRv = false;    // Boolean for timeout
    hDevNotify = 0;     // Handle of attach/detach notifier
    USBConnected = false;

    //Draw icon and label in the status bar
    statusLab = new QLabel() ;
    ledBar = new QToolButton() ;
    ledGray = QIcon(":/LED/Icons/Led_Gray.png");
    ledGreen = QIcon(":/LED/Icons/Led_G.png");
    ledYellow = QIcon(":/LED/Icons/Led_Y.png");
    ledRed = QIcon(":/LED/Icons/Led_R.png");
    ledGray.addPixmap(QPixmap(":/LED/Icons/Led_Gray.png"),QIcon::Disabled);
    ledGreen.addPixmap(QPixmap(":/LED/Icons/Led_G.png"),QIcon::Disabled);
    ledYellow.addPixmap(QPixmap(":/LED/Icons/Led_Y.png"),QIcon::Disabled);
    ledRed.addPixmap(QPixmap(":/LED/Icons/Led_R.png"),QIcon::Disabled);
    ledBar->setIcon(ledGray);
    ledBar->setEnabled(false);
    statusLab->setFixedWidth(100);
    statusBar()->addPermanentWidget(ledBar,100);
    statusBar()->addPermanentWidget(statusLab,100);

    //Check if our device is plugged
    USBConnected = USBCheck();

    //Connections for attach/detach notification
    connect(this, SIGNAL(USB_Arrived()),this, SLOT(in_USBArrived()));
    connect(this, SIGNAL(USB_Removed()),this, SLOT(in_USBRemoved()));

    //Receiving thread
    rthread = new ReadThread;
    connect(rthread, SIGNAL(USBReceived()),this, SLOT(in_USBReceived()));
    rthread->start();

}

/**************************************************
*                 DESTRUCTOR
***************************************************/
USBexample::~USBexample()
{
    if(hDevNotify)
        UnregisterDeviceNotification(hDevNotify);
    rthread->quit();
    delete ui;
}

/*******************************************
*             RECEIVING THREAD
********************************************/
void ReadThread::run()
 {
    DWORD BytesRead;

     while(1)
     {
       if(USBConnected && ReadHandle != INVALID_HANDLE_VALUE)
       {
         ReadFile(ReadHandle, &InputBuffer,65, &BytesRead, 0);
         emit   USBReceived();

       }
     }
 }
/*******************************************
*             RECEIVING SLOT
********************************************/
void USBexample::in_USBReceived()
{
    QString s;


    UsbRv = true;   //A packet was received

    /**********************************
    * Example 1. Print joystick position
    ***********************************/
    s = s.number((int)InputBuffer[1]);
    ui->Xlabel->setText(s);

}

/*******************************************
*              TIMEOUT SLOT
********************************************/
void USBexample::in_timerTimeout()
{
    TimerRv = true;
}

/*******************************************
*          USB ARRIVED SLOT
********************************************/
//Device arrived. Call USBCheck() to update WriteHandle, ReadHandle and status bar
void USBexample::in_USBArrived()
{
    USBConnected = USBCheck();
}

/*******************************************
*          USB REMOVED SLOT
********************************************/
void USBexample::in_USBRemoved()
{
    USBConnected = false;
    WriteHandle = INVALID_HANDLE_VALUE;
    ReadHandle = INVALID_HANDLE_VALUE;
    ledBar->setIcon(ledGray);
    statusLab->setText("Not Connected");
}

/*******************************************
*       Find HID Device by VID&PID
********************************************/
/* This function recursively scan all devices of HID class presents in the system
   and compares each VID&PID with our VID&PID. If the comparation is true it updates
   WriteHandle, ReadHandle and the status bar.
   Devices such as mice are read-only and can return an error
   when call CreateFile with GENERIC_WRITE parameter*/
bool USBexample::USBCheck()
{
    GUID InterfaceClassGuid = HID_CLASSGUID;
    HDEVINFO DeviceInfoTable = INVALID_HANDLE_VALUE;
    PSP_DEVICE_INTERFACE_DATA InterfaceDataStructure = new SP_DEVICE_INTERFACE_DATA;
    PSP_DEVICE_INTERFACE_DETAIL_DATA DetailedInterfaceDataStructure = new SP_DEVICE_INTERFACE_DETAIL_DATA;
    SP_DEVINFO_DATA DevInfoData;

    DWORD InterfaceIndex = 0;
    DWORD dwRegType;
    DWORD dwRegSize = 35;
    DWORD StructureSize = 0;
    PBYTE PropertyValueBuffer;
    bool DeviceFound = false;
    DWORD ErrorStatus;

    QString DeviceIDToFind = MY_DEVICE_VIDPID;

    if(WriteHandle!=INVALID_HANDLE_VALUE)
    {
       CloseHandle(WriteHandle);
       WriteHandle = INVALID_HANDLE_VALUE;
    }
    if(ReadHandle!=INVALID_HANDLE_VALUE)
    {
        CloseHandle(ReadHandle);
        ReadHandle = INVALID_HANDLE_VALUE;
    }
    DeviceInfoTable = SetupDiGetClassDevs(&InterfaceClassGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
    while(true)
    {
        InterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
        if(SetupDiEnumDeviceInterfaces(DeviceInfoTable, NULL, &InterfaceClassGuid, InterfaceIndex, InterfaceDataStructure))
        {
            ErrorStatus = GetLastError();
            if(ERROR_NO_MORE_ITEMS == ErrorStatus)
            {
                SetupDiDestroyDeviceInfoList(DeviceInfoTable);
                ledBar->setIcon(ledGray);
                statusLab->setText("Not Connected");
                return false;
            }
         }
         else
         {
            ErrorStatus = GetLastError();
            SetupDiDestroyDeviceInfoList(DeviceInfoTable);
            ledBar->setIcon(ledGray);
            statusLab->setText("Not Connected");
            return false;
         }
         DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
         SetupDiEnumDeviceInfo(DeviceInfoTable, InterfaceIndex, &DevInfoData);
         SetupDiGetDeviceRegistryPropertyA(DeviceInfoTable, &DevInfoData, SPDRP_HARDWAREID, &dwRegType, NULL, 0, &dwRegSize);
         PropertyValueBuffer = (BYTE *) malloc (dwRegSize);
         if(PropertyValueBuffer == NULL)
         {
            SetupDiDestroyDeviceInfoList(DeviceInfoTable);
            ledBar->setIcon(ledGray);
            statusLab->setText("Not Connected");
            return false;
         }
         SetupDiGetDeviceRegistryPropertyA(DeviceInfoTable, &DevInfoData, SPDRP_HARDWAREID, &dwRegType, PropertyValueBuffer, dwRegSize, NULL);
         QString DeviceIDFromRegistry((char *)PropertyValueBuffer);
         free(PropertyValueBuffer);
         DeviceIDFromRegistry = DeviceIDFromRegistry.toLower();
         DeviceIDToFind = DeviceIDToFind.toLower();
         DeviceFound = DeviceIDFromRegistry.contains(DeviceIDToFind);
         if(DeviceFound == true)
         {
            DetailedInterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
            SetupDiGetDeviceInterfaceDetail(DeviceInfoTable, InterfaceDataStructure, NULL, NULL, &StructureSize, NULL);
            DetailedInterfaceDataStructure = (PSP_DEVICE_INTERFACE_DETAIL_DATA)(malloc(StructureSize));
            if(DetailedInterfaceDataStructure == NULL)
            {
                SetupDiDestroyDeviceInfoList(DeviceInfoTable);
                ledBar->setIcon(ledGray);
                statusLab->setText("Not Connected");
                return false;
            }
            DetailedInterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
            SetupDiGetDeviceInterfaceDetail(DeviceInfoTable, InterfaceDataStructure, DetailedInterfaceDataStructure, StructureSize, NULL, NULL);
            WriteHandle = CreateFile((DetailedInterfaceDataStructure->DevicePath), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);
            ReadHandle = CreateFile((DetailedInterfaceDataStructure->DevicePath), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);
            if(WriteHandle==INVALID_HANDLE_VALUE || ReadHandle==INVALID_HANDLE_VALUE)
            {
               SetupDiDestroyDeviceInfoList(DeviceInfoTable);
               ledBar->setIcon(ledGray);
               statusLab->setText("Not Connected");
               return false;
            }
            SetupDiDestroyDeviceInfoList(DeviceInfoTable);
            ledBar->setIcon(ledGreen);
            statusLab->setText("Ready");
            return true;
         }
         InterfaceIndex++;
     }
    ledBar->setIcon(ledGray);
    statusLab->setText("Not Connected");
    return false;
}

/*******************************************
*             WRITE TO USB
********************************************/
int USBexample::USBWrite()
{
    DWORD BytesWritten = 0;

    WriteFile(WriteHandle, &OutputBuffer, 65, &BytesWritten, 0);
    return (int)BytesWritten;
}


/*******************************************
*             WINDOWS EVENTS
********************************************/
/*We use the first WM_PAINT event to get the handle of main window
  and pass it to RegisterDeviceNotification function.
  It not possible to do this in the contructor because the
  main window does not exist yet.
  WM_DEVICECHANGE event notify us that a device is attached or detached */
bool USBexample::winEvent(MSG * msg, long *result)
{
    qDebug() << "winEvent";
    int msgType = msg->message;
    if(msgType == WM_PAINT)
    {
         qDebug() << "WM_PAINT";
        if(!msgp)   //Only the first WM_PAINT
        {
            GUID InterfaceClassGuid = HID_CLASSGUID;
            DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
            ZeroMemory( &NotificationFilter, sizeof(NotificationFilter) );
            NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
            NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
            NotificationFilter.dbcc_classguid = InterfaceClassGuid;
          //  HWND hw = (HWND)this->effectiveWinId();   //Main window handle
            HWND hw = (HWND)this->winId();
            hDevNotify = RegisterDeviceNotification(hw,&NotificationFilter, DEVICE_NOTIFY_WINDOW_HANDLE);
            msgp = true;
        }
    }
    if(msgType == WM_DEVICECHANGE)
    {
        qDebug() << "dev change";
       PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)msg->lParam;
       switch(msg->wParam)
       {
            case DBT_DEVICEARRIVAL:
                if (lpdb -> dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
                {
                    PDEV_BROADCAST_DEVICEINTERFACE lpdbv = (PDEV_BROADCAST_DEVICEINTERFACE)lpdb;
                    int i = 0;
                    QString s;
                    //to find a better way for this...
                    while(lpdbv->dbcc_name[i] != 0)
                    {
                        s.append(lpdbv->dbcc_name[i]);
                        i++;
                    }
                    s = s.toUpper();
                    if(s.contains(MY_DEVICE_VIDPID))
                        emit USB_Arrived();
                }
            break;
            case DBT_DEVICEREMOVECOMPLETE:
                if (lpdb -> dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
                {
                    PDEV_BROADCAST_DEVICEINTERFACE lpdbv = (PDEV_BROADCAST_DEVICEINTERFACE)lpdb;
                    int i = 0;
                    QString s;
                    //to find a better way for this...
                    while(lpdbv->dbcc_name[i] != 0)
                    {
                        s.append(lpdbv->dbcc_name[i]);
                        i++;
                    }
                    s = s.toUpper();
                    if(s.contains(MY_DEVICE_VIDPID))
                        emit USB_Removed();
                }
            break;


       }
    }
    return false;
}



/*******************************************
*             WRITE/READ BUTTON
********************************************/

void USBexample::on_checkBoxGreen_clicked()
{
    if(USBConnected)
    {
        ledBar->setIcon(ledYellow);
        OutputBuffer[0] = 0;    //First byte is for Windows internal use and it is always 0
        if (ui->checkBoxGreen->isChecked()) OutputBuffer[1] = 'A';
         else OutputBuffer[1] = 'B';
        ledBar->setIcon(ledGreen);
        USBWrite();
    }
}

void USBexample::on_checkBoxRed_clicked()
{
    if(USBConnected)
    {
        ledBar->setIcon(ledYellow);
        OutputBuffer[0] = 0;    //First byte is for Windows internal use and it is always 0
        if (ui->checkBoxRed->isChecked()) OutputBuffer[1] = 'C';
         else OutputBuffer[1] = 'D';
        ledBar->setIcon(ledGreen);
        USBWrite();
    }
}
